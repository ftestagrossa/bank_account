import sbt.Keys._

lazy val akkaVersion = "2.5.25"
lazy val root = (project in file("."))
  .enablePlugins(PlayService, PlayLayoutPlugin)
  .settings(
    name := "distillery_interview",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.0",
    resolvers ++= Seq(
      Resolver sonatypeRepo "public",
      Resolver typesafeRepo "releases",
      Resolver.bintrayRepo("tanukkii007", "maven"),
      // the library is available in Bintray repository
      "dnvriend" at "http://dl.bintray.com/dnvriend/maven"
    ),
    libraryDependencies ++= Seq(
      guice,
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "net.codingwell" %% "scala-guice" % "4.2.5",
      "org.scalaz" %% "scalaz-core" % "7.2.28",
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence-cassandra" % "0.98",
      "com.github.dnvriend" %% "akka-persistence-inmemory" % "2.5.15.2",
      "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test
    ),
    Test / parallelExecution := false,
    Test / fork := false,
    Test / javaOptions += "-Xmx2G",
    coverageExcludedPackages := "router.Routes.*;<empty>;Reverse.*;router\\.*"
  )

addCommandAlias("c", "compile")
addCommandAlias("s", "scalastyle")
addCommandAlias("tc", "test:compile")
addCommandAlias("ts", "test:scalastyle")
addCommandAlias("t", "test")
addCommandAlias("to", "testOnly")
