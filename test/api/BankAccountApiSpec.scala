package api

import api.Module.BankAccountSystem
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.play.PlaySpec
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.AnyContentAsEmpty
import play.api.test.FakeRequest
import play.api.test.Helpers.{GET => GET_REQUEST, _}
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

class BankAccountApiSpec extends PlaySpec with BeforeAndAfterEach {
  import BankAccountApiSpec._

  implicit var app: Application = _
  implicit var apiSystem: BankAccountSystem = _

  // we need to terminate the system after each test in order to provide resource clean up
  // tried to use https://github.com/dnvriend/akka-persistence-inmemory#clearing-journal-and-snapshot-messages
  // but did not worked
  override def afterEach(): Unit = {
    Await.result(apiSystem.system.terminate(), 1 seconds)
    super.afterEach()
  }

  override def beforeEach(): Unit = {
    app = new GuiceApplicationBuilder().build()
    apiSystem = app.injector.instanceOf(classOf[BankAccountSystem])
    super.beforeEach()
  }

  def balanceSuccess(amount: Int): Unit = {
    val balanceResult = route(app, Balance).get
    status(balanceResult) must be(OK)
    (contentAsJson(balanceResult) \ "balance").as[BigInt] must be(amount)
  }
  def depositSuccess(id: Int, amount: Int): Unit = {
    val jsonDeposit = Json.obj("id" -> id, "amount" -> amount)
    val depositResult = route(app, Deposit.withBody(jsonDeposit)).get
    status(depositResult) must be(OK)
    (contentAsJson(depositResult) \ "id").as[BigInt] must be(id)
  }
  def withdrawSuccess(id: Int, amount: Int): Unit = {
    val jsonWithdraw = Json.obj("id" -> id, "amount" -> amount)
    val withdrawResult = route(app, Withdraw.withBody(jsonWithdraw)).get
    status(withdrawResult) must be(OK)
    (contentAsJson(withdrawResult) \ "id").as[BigInt] must be(id)
  }

  "Health" should {
    "result Ok" in {
      val healthResult = route(app, Health).get
      status(healthResult) must be(OK)
      contentAsString(healthResult) must be("healthy")
    }
  }

  "Sending a wrong DepositRequest" should {
    "fail" in {
      val jsonDeposit = Json.obj("id" -> 1, "amount" -> "40001.00")
      val depositResult = route(app, Deposit.withBody(jsonDeposit)).get
      status(depositResult) must be(UNPROCESSABLE_ENTITY)
      (contentAsJson(depositResult) \ "error").as[String] must be(
        "Exceeded Maximum Deposit Per Transaction"
      )
    }
  }
  "Sending a wrong WithdrawRequest" should {
    "fail" in {
      val jsonWithdraw = Json.obj("id" -> 1, "amount" -> "20001.00")
      val withdrawResult = route(app, Withdraw.withBody(jsonWithdraw)).get
      status(withdrawResult) must be(UNPROCESSABLE_ENTITY)
      (contentAsJson(withdrawResult) \ "error").as[String] must be(
        "Exceeded Maximum Withdraw Per Transaction"
      )
    }
  }

  "Depositing and then Withdrawing the same ammount" should {
    "return a balance equals 0" in {
      depositSuccess(1, 100)
      balanceSuccess(100)
      withdrawSuccess(1, 100)
      balanceSuccess(0)
    }
  }

  "Calling deposit over the date limit" should {
    "fail due to frequency exceeded by date" in {
      depositSuccess(1, 100)
      depositSuccess(2, 100)
      depositSuccess(3, 100)
      depositSuccess(4, 100)
      val jsonDeposit = Json.obj("id" -> 5, "amount" -> "100")
      val depositResult = route(app, Deposit.withBody(jsonDeposit)).get
      status(depositResult) must be(CONFLICT)
      (contentAsJson(depositResult) \ "error").as[String] must be(
        "Exceeded Maximum Deposit Frequency Per Day"
      )
      (contentAsJson(depositResult) \ "solution").as[String] must be(
        "Try again tomorrow"
      )
    }
  }
  "Calling withdraw over the date limit" should {
    "fail due to frequency exceeded by date" in {
      depositSuccess(1, 1000)
      withdrawSuccess(2, 100)
      withdrawSuccess(3, 100)
      withdrawSuccess(4, 100)
      val jsonWithdraw = Json.obj("id" -> 5, "amount" -> "100")
      val withdrawResult = route(app, Withdraw.withBody(jsonWithdraw)).get
      status(withdrawResult) must be(CONFLICT)
      (contentAsJson(withdrawResult) \ "error").as[String] must be(
        "Exceeded Maximum Withdraw Frequency Per Day"
      )
      (contentAsJson(withdrawResult) \ "solution").as[String] must be(
        "Try again tomorrow"
      )
    }
  }

  "Depositing over the amount date limit" should {
    "fail due to deposit amount per day exceeded" in {
      depositSuccess(1, 40000)
      depositSuccess(2, 40000)
      depositSuccess(3, 40000)
      val jsonDeposit = Json.obj("id" -> 4, "amount" -> "30001")
      val depositResult = route(app, Deposit.withBody(jsonDeposit)).get
      status(depositResult) must be(CONFLICT)
      (contentAsJson(depositResult) \ "error").as[String] must be(
        "Exceeded Maximum Deposit Per Day"
      )
      (contentAsJson(depositResult) \ "solution").as[String] must be(
        "Deposit as maximum as 30000.00 amount or try again tomorrow"
      )
    }
  }
  "Withdrawing over the amount date limit" should {
    "fail due to deposit amount per day exceeded" in {
      depositSuccess(1, 40000)
      depositSuccess(2, 40000)
      withdrawSuccess(3, 20000)
      withdrawSuccess(4, 20000)
      val jsonWithdraw = Json.obj("id" -> 5, "amount" -> "10001")
      val withdrawResult = route(app, Withdraw.withBody(jsonWithdraw)).get
      status(withdrawResult) must be(CONFLICT)
      (contentAsJson(withdrawResult) \ "error").as[String] must be(
        "Exceeded Maximum Withdraw Per Day"
      )
      (contentAsJson(withdrawResult) \ "solution").as[String] must be(
        "Withdraw as maximum as 10000.00 amount or try again tomorrow"
      )
    }
  }

  "Balance" should {
    "not be negative" in {
      val jsonWithdraw = Json.obj("id" -> 1, "amount" -> "1")
      val withdrawResult = route(app, Withdraw.withBody(jsonWithdraw)).get
      status(withdrawResult) must be(CONFLICT)
      (contentAsJson(withdrawResult) \ "error").as[String] must be(
        "BankAccount balance cannot be negative"
      )
      (contentAsJson(withdrawResult) \ "solution").as[String] must be(
        "Withdraw as maximum as 0.0 amount"
      )
    }
  }
}

object BankAccountApiSpec {
  val root = "/account"
  def health: String = "/health"
  def balance: String = s"$root/balance"
  def deposit: String = s"$root/deposit"
  def withdraw: String = s"$root/withdraw"

  def Health: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(GET_REQUEST, health)
  def Balance: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(GET_REQUEST, balance)
  def Deposit: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(POST, deposit)
  def Withdraw: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(POST, withdraw)
}