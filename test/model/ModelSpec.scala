package model

import java.time.LocalDateTime

import model.BankAccountError._
import model.transaction._
import model.transaction.TransactionError._
import org.scalatest.{MustMatchers, WordSpec}

class ModelSpec extends WordSpec with MustMatchers {

  "Try to create a Deposit with amount > than max amount per transaction" should {
    "fail with error: TransactionAmountExceeded" in {
      Deposit.create(BigInt("1"), LocalDateTime.now(), BigDecimal("40001.00")) must be(
        Left(TransactionAmountExceeded("Exceeded Maximum Deposit Per Transaction"))
      )
    }
  }
  "Try to create a Withdraw with amount > than max amount per transaction" should {
    "fail with error: TransactionAmountExceeded" in {
      Withdraw.create(BigInt("1"), LocalDateTime.now(), BigDecimal("20001.00")) must be(
        Left(TransactionAmountExceeded("Exceeded Maximum Withdraw Per Transaction"))
      )
    }
  }

  def deposit(id: BigInt,
              date: LocalDateTime,
              amount: BigDecimal): Transaction = Deposit.create(id, date, amount).toOption.get

  def withdraw(id: BigInt,
               date: LocalDateTime,
               amount: BigDecimal): Transaction = Withdraw.create(id, date, amount).toOption.get

  "The maxiumum number of deposits per day for a bank account" should {
    "be 4" in {
      val a0 = BankAccount.init()

      val today = LocalDateTime.now
      val successResult: Either[BankAccountError, BankAccount] = for {
        a1 <- a0 +> deposit(BigInt("1"), today, BigDecimal("1.00"))
        a2 <- a1 +> deposit(BigInt("2"), today, BigDecimal("1.00"))
        a3 <- a2 +> deposit(BigInt("3"), today, BigDecimal("1.00"))
        a4 <- a3 +> deposit(BigInt("4"), today, BigDecimal("1.00"))
      } yield a4

      val expectedBankAccount = BankAccount(BigDecimal("4.00"), Seq(
        deposit(BigInt("1"), today, BigDecimal("1.00")),
        deposit(BigInt("2"), today, BigDecimal("1.00")),
        deposit(BigInt("3"), today, BigDecimal("1.00")),
        deposit(BigInt("4"), today, BigDecimal("1.00"))
      ))

      successResult must be (Right(expectedBankAccount))

      val a4 = successResult.toOption.get

      val failResult = a4 +> deposit(BigInt("5"), today, BigDecimal("1.00"))

      failResult match {
        case Left(MaximumTransactionsFrequencyPerDayExceeded(msg, solution)) =>
          msg must be("Exceeded Maximum Deposit Frequency Per Day")
          solution must be("Try again tomorrow")
        case _ =>
          assert(false)
      }
    }
  }
  "The maxiumum number of withdraws per day for a bank account" should {
    "be 3" in {
      val a0 = BankAccount.init()

      val today = LocalDateTime.now
      val successResult: Either[BankAccountError, BankAccount] = for {
        a1 <- a0 +> deposit(BigInt("1"), today, BigDecimal("4.00"))
        a2 <- a1 +> withdraw(BigInt("2"), today, BigDecimal("1.00"))
        a3 <- a2 +> withdraw(BigInt("3"), today, BigDecimal("1.00"))
        a4 <- a3 +> withdraw(BigInt("4"), today, BigDecimal("1.00"))
      } yield a4

      val expectedBankAccount = BankAccount(BigDecimal("1.00"), Seq(
        deposit(BigInt("1"), today, BigDecimal("4.00")),
        withdraw(BigInt("2"), today, BigDecimal("1.00")),
        withdraw(BigInt("3"), today, BigDecimal("1.00")),
        withdraw(BigInt("4"), today, BigDecimal("1.00"))
      ))

      successResult must be (Right(expectedBankAccount))

      val a3 = successResult.toOption.get

      val failResult = a3 +> withdraw(BigInt("5"), today, BigDecimal("1.00"))

      failResult match {
        case Left(MaximumTransactionsFrequencyPerDayExceeded(msg, solution)) =>
          msg must be("Exceeded Maximum Withdraw Frequency Per Day")
          solution must be("Try again tomorrow")
        case _ =>
          assert(false)
      }
    }
  }
  "The maxiumum deposits per day for a bank account" should {
    "be 150000" in {
      val a0 = BankAccount.init()

      val today = LocalDateTime.now
      val expectedBankAccount = BankAccount(BigDecimal("120000.00"), Seq(
        deposit(BigInt("1"), today, BigDecimal("40000.00")),
        deposit(BigInt("2"), today, BigDecimal("40000.00")),
        deposit(BigInt("3"), today, BigDecimal("40000.00"))
      ))

      val successResult: Either[BankAccountError, BankAccount] = for {
        a1 <- a0 +> expectedBankAccount.transactions(0)
        a2 <- a1 +> expectedBankAccount.transactions(1)
        a3 <- a2 +> expectedBankAccount.transactions(2)
      } yield a3

      successResult must be (Right(expectedBankAccount))

      val a3 = successResult.toOption.get

      val failResult = a3 +> deposit(BigInt("4"), today, BigDecimal("30001.00"))

      failResult match {
        case Left(MaximumTransactionsPerDayExceeded(msg, solution)) =>
          msg must be("Exceeded Maximum Deposit Per Day")
          solution must be("Deposit as maximum as 30000.00 amount or try again tomorrow")
        case _ =>
          assert(false)
      }
    }
  }
  "The maxiumum withdraws per day for a bank account" should {
    "be 50000" in {
      val a0 = BankAccount.init()

      val today = LocalDateTime.now
      val expectedBankAccount = BankAccount(BigDecimal("40000.00"), Seq(
        deposit(BigInt("1"), today, BigDecimal("40000.00")),
        deposit(BigInt("2"), today, BigDecimal("40000.00")),
        withdraw(BigInt("3"), today, BigDecimal("20000.00")),
        withdraw(BigInt("4"), today, BigDecimal("20000.00"))
      ))

      val successResult: Either[BankAccountError, BankAccount] = for {
        a1 <- a0 +> expectedBankAccount.transactions(0)
        a2 <- a1 +> expectedBankAccount.transactions(1)
        a3 <- a2 +> expectedBankAccount.transactions(2)
        a4 <- a3 +> expectedBankAccount.transactions(3)
      } yield a4

      successResult must be (Right(expectedBankAccount))

      val a4 = successResult.toOption.get

      val failResult = a4 +> withdraw(BigInt("5"), today, BigDecimal("10001.00"))

      failResult match {
        case Left(MaximumTransactionsPerDayExceeded(msg, solution)) =>
          msg must be("Exceeded Maximum Withdraw Per Day")
          solution must be("Withdraw as maximum as 10000.00 amount or try again tomorrow")
        case _ =>
          assert(false)
      }
    }
  }
  "Withdraw from a bank account with an ammount bigger than balance" should {
    "fail with WithdrawNotAccepted" in {
      val a0 = BankAccount.init()

      val today = LocalDateTime.now
      val expectedBankAccount = BankAccount(BigDecimal("10000.00"), Seq(
        deposit(BigInt("1"), today, BigDecimal("10000.00"))
      ))

      val successResult: Either[BankAccountError, BankAccount] = for {
        a1 <- a0 +> expectedBankAccount.transactions(0)
      } yield a1

      successResult must be (Right(expectedBankAccount))

      val a1 = successResult.toOption.get

      val failResult = a1 +> withdraw(BigInt("2"), today, BigDecimal("10001.00"))

      failResult match {
        case Left(WithdrawNotAccepted(msg, solution)) =>
          msg must be("BankAccount balance cannot be negative")
          solution must be("Withdraw as maximum as 10000.00 amount")
        case _ =>
          assert(false)
      }
    }
  }
}
