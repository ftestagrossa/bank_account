package model

import java.time.LocalDateTime

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKitBase}
import akka.util.Timeout
import api.Module.BankAccountSystem
import model.BankAccountAggregate.{BalanceResponse, DepositSucceed, GetBalance, WithdrawSucceed}
import model.BankAccountError.WithdrawNotAccepted
import model.transaction.{Deposit, Withdraw}
import org.scalatest.{MustMatchers, WordSpecLike}

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.math.BigInt

class BankAccountAggregateSpec extends TestKitBase
  with WordSpecLike
  with MustMatchers
  with ImplicitSender {

  implicit lazy val system: ActorSystem = BankAccountSystem(ActorSystem("BankAccount")).system
  implicit private val timeout: Timeout = 7 seconds

  "The BankAccountAggregate" should {
    "finish with balance 0.00" in {
      val bankAccount = childActorOf(BankAccountAggregate.props())

      bankAccount ! Deposit(BigInt("1"), LocalDateTime.now(), BigDecimal("100.00"))
      bankAccount ! GetBalance
      bankAccount ! Deposit(BigInt("2"), LocalDateTime.now(), BigDecimal("100.00"))
      bankAccount ! GetBalance
      bankAccount ! Withdraw(BigInt("3"), LocalDateTime.now(), BigDecimal("100.00"))
      bankAccount ! GetBalance
      bankAccount ! Withdraw(BigInt("4"), LocalDateTime.now(), BigDecimal("100.00"))
      bankAccount ! GetBalance
      bankAccount ! Withdraw(BigInt("5"), LocalDateTime.now(), BigDecimal("100.00"))
      bankAccount ! GetBalance

      within(5 seconds) {
        expectMsgPF() {
          case DepositSucceed(id) if id == 1 => true
          case BalanceResponse(balance) if balance == 100.00 => true
          case DepositSucceed(id) if id == 2 => true
          case BalanceResponse(balance) if balance == 200.00 => true
          case WithdrawSucceed(id) if id == 3 => true
          case BalanceResponse(balance) if balance == 100.00 => true
          case WithdrawSucceed(id) if id == 4 => true
          case BalanceResponse(balance) if balance == 0.00 => true
          case akka.actor.Status.Failure(WithdrawNotAccepted(_, _)) => true
        }
      }
    }
  }
}
