package api

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import javax.inject.{Inject, Named}
import model.{BankAccountAggregate, BankAccountError}
import model.BankAccountAggregate.{BalanceResponse, DepositSucceed, WithdrawSucceed}
import model.transaction.{Deposit, TransactionError, Withdraw}
import play.api.libs.json.{Json, JsValue}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

class BankAccountController @Inject()(
                                      cc: ControllerComponents,
                                      @Named("BankAccount") bankAccount: ActorRef
                                    )(implicit ec: ExecutionContext)
  extends AbstractController(cc) {

  implicit private val timeout: Timeout = 10 seconds // default

  def getBalance(): Action[AnyContent] = Action.async {
    (bankAccount ? BankAccountAggregate.GetBalance)
      .mapTo[BalanceResponse]
      .map { response =>
        Ok(Json.toJson(response))
      }
  }

  def deposit(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    val depositRqst = request.body.as[DepositRequest]
    Deposit.create(depositRqst.id, depositRqst.date, depositRqst.amount) match {
      case Left(value: TransactionError) =>
        Future(UnprocessableEntity(Json.toJson(value)))
      case Right(deposit) =>
        (bankAccount ? deposit)
          .mapTo[DepositSucceed]
          .map { response =>
            Ok(Json.toJson(response))
          }
          .recover {
            case transactionError: TransactionError =>
              Conflict(Json.toJson(transactionError))
            case bankAccountError: BankAccountError =>
              Conflict(Json.toJson(bankAccountError))
          }
    }
  }

  def withdraw(): Action[JsValue] = Action.async(parse.json) { implicit request =>
    val withdrawRqst = request.body.as[WithdrawRequest]
    Withdraw.create(withdrawRqst.id, withdrawRqst.date, withdrawRqst.amount) match {
      case Left(value: TransactionError) =>
        Future(UnprocessableEntity(Json.toJson(value)))
      case Right(withdraw) =>
        (bankAccount ? withdraw)
          .mapTo[WithdrawSucceed]
          .map { response =>
            Ok(Json.toJson(response))
          }
          .recover {
            case transactionError: TransactionError =>
              Conflict(Json.toJson(transactionError))
            case bankAccountError: BankAccountError =>
              Conflict(Json.toJson(bankAccountError))
          }
    }
  }
}
