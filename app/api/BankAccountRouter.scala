package api

import javax.inject.Inject

import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

class BankAccountRouter @Inject()(bankAccountController: BankAccountController) extends SimpleRouter  {

  def routes: Routes = {
    case GET(p"/balance") => bankAccountController.getBalance()
    case POST(p"/deposit") => bankAccountController.deposit()
    case POST(p"/withdraw") => bankAccountController.withdraw()
  }
}
