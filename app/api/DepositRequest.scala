package api

import java.time.LocalDateTime

import play.api.libs.json.Json

case class DepositRequest(id: BigInt, amount: BigDecimal) {
  def date: LocalDateTime = LocalDateTime.now()
}
object DepositRequest {
  implicit val depositRequestF = Json.format[DepositRequest]
}