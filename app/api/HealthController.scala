package api

import javax.inject.Inject

import scala.concurrent.ExecutionContext

import play.api.mvc.{Action, AnyContent, InjectedController}


class HealthController @Inject()(implicit ec: ExecutionContext) extends InjectedController {

  def health: Action[AnyContent] = Action { Ok("healthy") }
}