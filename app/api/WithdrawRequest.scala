package api

import java.time.LocalDateTime

import play.api.libs.json.Json

case class WithdrawRequest(id: BigInt, amount: BigDecimal) {
  def date: LocalDateTime = LocalDateTime.now()
}
object WithdrawRequest {
  implicit val withdrawRequestF = Json.format[WithdrawRequest]
}
