package api

import akka.actor.{ActorRef, ActorSystem, CoordinatedShutdown}
import api.Module.BankAccountSystem
import com.google.inject.AbstractModule
import com.google.inject.name.Names
import model.BankAccountAggregate
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}

class Module(environment: Environment, configuration: Configuration) extends AbstractModule with ScalaModule {
  override def configure(): Unit = {
    implicit val system: ActorSystem = ActorSystem("BankAccount")
    bind(classOf[BankAccountSystem]).toInstance(new BankAccountSystem(system))

    val bankAccount = system.actorOf(BankAccountAggregate.props())
    binder.bind(classOf[ActorRef])
      .annotatedWith(Names.named("BankAccount"))
      .toInstance(bankAccount)

    CoordinatedShutdown(system).addJvmShutdownHook(
      println("BankAccount system is shutting down.")
    )
  }
}
object Module {
  case class BankAccountSystem(system: ActorSystem)
}
