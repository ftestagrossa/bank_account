package model

import play.api.libs.json.{Json, Writes}

sealed trait BankAccountError extends Exception {
  def msg: String
  def solution: String
}
object BankAccountError {
  final case class WithdrawNotAccepted(msg: String, solution: String) extends BankAccountError
  final case class MaximumTransactionsPerDayExceeded(msg: String,
                                                     solution: String
                                                    ) extends BankAccountError
  final case class MaximumTransactionsFrequencyPerDayExceeded(msg: String,
                                                              solution: String = "Try again tomorrow"
                                                             ) extends BankAccountError

  implicit val bankAccountErrorW: Writes[BankAccountError] = {
    case error: BankAccountError => Json.obj(
      "error" -> error.msg,
      "solution" -> error.solution
    )
  }
}

