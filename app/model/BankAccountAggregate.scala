package model

import akka.actor.{ActorLogging, Props}
import akka.persistence.PersistentActor
import model.transaction._
import play.api.libs.json.Json

class BankAccountAggregate extends PersistentActor with ActorLogging {

  override def persistenceId: String = "BankAccountAggregate"

  var account: BankAccount = BankAccount.init()

  import BankAccountAggregate._
  
  override def receiveRecover: Receive = {
    case e: Event => account += e
  }

  override def receiveCommand: Receive = {
    case GetBalance =>
      sender() ! BalanceResponse(account.balance)
    case w: Withdraw =>
      account +> w match {
        case Left(error) => sender() ! akka.actor.Status.Failure(error)
        case Right(_) =>
          val evt = Withdrawn(w)
          persist(evt) { e =>
            account += e
            sender() ! WithdrawSucceed(w.id)
          }
      }
    case d: Deposit =>
      account +> d match {
        case Left(error) => sender() ! akka.actor.Status.Failure(error)
        case Right(_) =>
          val evt = Deposited(d)
          persist(evt) { e =>
            account += e
            sender() ! DepositSucceed(d.id)
          }
      }
  }
}

object BankAccountAggregate {
  def props(): Props = Props(new BankAccountAggregate)

  sealed trait Command
  case object GetBalance extends Command

  sealed trait Response
  case class BalanceResponse(balance: BigDecimal) extends Response
  case class DepositSucceed(id: BigInt) extends Response
  case class WithdrawSucceed(id: BigInt) extends Response
  object Response {
    implicit val balanceResponseF = Json.format[BalanceResponse]
    implicit val depositSucceedF = Json.format[DepositSucceed]
    implicit val withdrawSucceedF = Json.format[WithdrawSucceed]
  }

  sealed trait Event
  case class Deposited(deposit: Deposit) extends Event
  case class Withdrawn(withdraw: Withdraw) extends Event
}
