package model.transaction

import java.time.LocalDateTime

final case class Withdraw(id: BigInt,
                          date: LocalDateTime,
                          amount: BigDecimal) extends Transaction {
  override def amountValue: BigDecimal = -amount
  override def name: String = "Withdraw"
  override def maxAmountPerTransaction: BigDecimal = BigDecimal("20000.00")
  override def maxAmountPerDay: BigDecimal = BigDecimal("50000.00")
  override def maxFrequencyPerDay: BigInt = BigInt("3")
}

object Withdraw {
  def create(id: BigInt,
             date: LocalDateTime,
             amount: BigDecimal): Either[TransactionError, Transaction] = {
    Transaction.verifyTransactionAmount(Withdraw(id, date, amount))
  }
}