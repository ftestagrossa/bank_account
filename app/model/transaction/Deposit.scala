package model.transaction

import java.time.LocalDateTime

final case class Deposit private (id: BigInt,
                                  date: LocalDateTime,
                                  amount: BigDecimal) extends Transaction {
  override def amountValue: BigDecimal = amount
  override def name: String = "Deposit"
  override def maxAmountPerTransaction: BigDecimal = BigDecimal("40000.00")
  override def maxAmountPerDay: BigDecimal = BigDecimal("150000.00")
  override def maxFrequencyPerDay: BigInt = BigInt("4")
}

object Deposit {
  def create(id: BigInt,
             date: LocalDateTime,
             amount: BigDecimal): Either[TransactionError, Transaction] = {
    Transaction.verifyTransactionAmount(Deposit(id, date, amount))
  }
}