package model.transaction

import play.api.libs.json.{Json, Writes}

sealed trait TransactionError extends Exception {
  def msg: String
}
object TransactionError {
  final case class TransactionAmountExceeded(msg: String) extends TransactionError

  implicit val transactionErrorW: Writes[TransactionError] = {
    case error: TransactionError => Json.obj(
      "error" -> error.msg
    )
  }
}
