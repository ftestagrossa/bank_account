package model.transaction

import java.time.LocalDateTime

import model.transaction.TransactionError.TransactionAmountExceeded

trait Transaction extends Product with Serializable {
  def id: BigInt
  def date: LocalDateTime
  def amount: BigDecimal
  def name: String
  def amountValue: BigDecimal // the value representation
  // constraints
  def maxAmountPerTransaction: BigDecimal
  def maxAmountPerDay: BigDecimal
  def maxFrequencyPerDay: BigInt
}

object Transaction {
  def verifyTransactionAmount(transaction: Transaction): Either[TransactionError, Transaction] =
    if (transaction.amount > transaction.maxAmountPerTransaction) {
      Left(TransactionAmountExceeded(s"Exceeded Maximum ${transaction.name} Per Transaction"))
    }
    else {
      Right(transaction)
    }
}


