package model

import model.BankAccountError._
import model.transaction.{Deposit, Transaction, Withdraw}

final case class BankAccount private (balance: BigDecimal,
                                      transactions: Seq[Transaction]) {
  private def transactionsByDay(transaction: Transaction, dayOfYear: Int): Seq[Transaction] =
    transaction match {
      case _: Deposit =>
        transactions.collect {
          case d@Deposit(_, date, _) if date.getDayOfYear == dayOfYear => d
        }
      case _: Withdraw =>
        transactions.collect {
          case d@Withdraw(_, date, _) if date.getDayOfYear == dayOfYear => d
        }
    }

  private def verifyMaxTransactionByDay(transaction: Transaction): Either[BankAccountError, Unit] = {
    val today = transaction.date.getDayOfYear
    val newAmount = transactionsByDay(transaction, today)
      .map(_.amount)
      .sum + transaction.amount
    if (newAmount > transaction.maxAmountPerDay) {
      Left(MaximumTransactionsPerDayExceeded(s"Exceeded Maximum ${transaction.name} Per Day",
        s"${transaction.name} as maximum as ${
          transaction.amount - (newAmount - transaction.maxAmountPerDay)
        } amount or try again tomorrow"))
    }
    else {
      Right(())
    }
  }

  private def verifyTransactionFrequencyByDay(transaction: Transaction): Either[BankAccountError, Unit] = {
    val today = transaction.date.getDayOfYear
    if ((transactionsByDay(transaction, today).size + 1) > transaction.maxFrequencyPerDay) {
      Left(MaximumTransactionsFrequencyPerDayExceeded(s"Exceeded Maximum ${transaction.name} Frequency Per Day"))
    }
    else {
      Right(())
    }
  }

  private def verifyBalance(transaction: Transaction): Either[BankAccountError, Unit] = {
    transaction match {
      case Withdraw(_, _, amount) if (balance - amount) < 0 =>
        Left(WithdrawNotAccepted("BankAccount balance cannot be negative",
        s"Withdraw as maximum as $balance amount"))
      case _ =>
        Right(())
    }
  }

  def +> (transaction: Transaction): Either[BankAccountError, BankAccount] =
    for {
      _ <- verifyMaxTransactionByDay(transaction)
      _ <- verifyTransactionFrequencyByDay(transaction)
      _ <- verifyBalance(transaction)
    } yield copy(
      balance = this.balance + transaction.amountValue,
      transactions = this.transactions :+ transaction
    )

  def +(event: BankAccountAggregate.Event): BankAccount = event match {
    case BankAccountAggregate.Deposited(deposit) =>
      copy(
        balance = this.balance + deposit.amountValue,
        transactions = this.transactions :+ deposit
      )
    case BankAccountAggregate.Withdrawn(withdraw) =>
      copy(
        balance = this.balance + withdraw.amountValue,
        transactions = this.transactions :+ withdraw
      )
  }
}

object BankAccount {
  def init(): BankAccount = BankAccount(BigDecimal("0.0"), Seq.empty)
}
