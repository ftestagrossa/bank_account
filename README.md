# Problem to solve
## Backend Mini Project (Bank Account)
The goal of this mini project is to write a simple micro web service to mimic a “Bank Account”. Through this web service, one can query about the balance, deposit money, and withdraw money. Just like any Bank, there are restrictions on how many transactions/amounts it can handle. The details are described below.

* Write a simple “Bank Account” web service using REST API design principles. You can use either the Scala/Play or Java/Spring/Tomcat framework.

* Program should have 3 REST API endpoints: Balance, Deposit, and Withdrawal

* No requirement for authentication ­ assume the web service is for one account only and is open to the world

* No requirement for the backend store ­ you can store it in a file or database (your decision)

* Balance endpoint ­ this will return the outstanding balance

* Deposit endpoint ­ credits the account with the specified amount

    * Max deposit for the day = $150K

    * Max deposit per transaction = $40K

    * Max deposit frequency = 4 transactions/day

* Withdrawal endpoint ­ deducts the account with the specified amount

    * Max withdrawal for the day = $50K

    * Max withdrawal per transaction = $20K

    * Max withdrawal frequency = 3 transactions/day

    * Cannot withdraw when balance is less than withdrawal amount

* The service should handle all the error cases and return the appropriate error HTTP status code and error message (Eg. If an attempt is to withdraw greater than $20k in a single transaction, the error message should say “Exceeded Maximum Withdrawal Per Transaction”).

* Write tests against your web service. ( Bonus: implement a code coverage tool and show code coverage numbers for your tests)

* Make sure your code is readable and can be run.

* Check in your code to bitbucket and write instructions on readme on how to run.

Share with us the bitbucket and we will review your project from there.

# Run test coverage
sbt clean coverage test coverageReport

# Run the application listening on port 9000
sbt run

# Helpful curls to the application
curl -v -X GET http://localhost:9000/health
curl -v -X GET http://localhost:9000/account/balance
curl -v -X POST http://localhost:9000/account/deposit -H "Content-Type: application/json" -d '{"id": 1, "amount": 100}'
curl -v -X POST http://localhost:9000/account/withdraw -H "Content-Type: application/json" -d '{"id": 1, "amount": 100}'
