addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.6.0-RC1")
// Coverage: sbt clean coverage test coverageReport
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.3")